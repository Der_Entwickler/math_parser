package com.example.math_parther;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {

    private int HEIGHT = 520;
    private int WIDTH = 800;

    private Circle circle;
    private Line coordinateY;
    private Line coordinateX;

    @Override
    public void start(Stage stage) throws IOException {


        Group group = new Group();

        coordinateX = new Line(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
        coordinateY = new Line(WIDTH / 2, HEIGHT, WIDTH / 2, 0);

        group.getChildren().addAll(
                coordinateX,
                coordinateY
        );

        circle = new Circle(50);
        circle.setFill(Color.BLUE);
        Pane stackPane = new Pane();
        stackPane.getChildren().add(coordinateX);
        stackPane.getChildren().add(coordinateY);



        Pane stackPane2 = new Pane();


        Button button = new Button("Change");
        StackPane stackPane1 = new StackPane();
        stackPane1.getChildren().add(button);



        BorderPane pane = new BorderPane();
        pane.setCenter(stackPane);
        pane.setBottom(stackPane1);
        button.setOnKeyPressed(new ButtonListener());

        Scene scene = new Scene(pane, WIDTH, HEIGHT);

        stage.setScene(scene);
        stage.show();

    }

    class ButtonListener implements EventHandler<KeyEvent> {

        @Override
        public void handle(KeyEvent event) {

            if (event.getCode() == KeyCode.RIGHT) {
                coordinateY.setStartX(coordinateY.getStartX()-5);
                coordinateY.setEndX(coordinateY.getEndX()-5);
                System.out.println(coordinateX.getStartX());
            }
            else if(event.getCode() == KeyCode.LEFT) {
                coordinateY.setStartX(coordinateY.getStartX()+5);
                coordinateY.setEndX(coordinateY.getEndX()+5);
            }
            else if(event.getCode() == KeyCode.UP) {
                coordinateX.setStartY(coordinateX.getStartY()+5);
                coordinateX.setEndY(coordinateX.getEndY()+5);
            }
            else if(event.getCode() == KeyCode.DOWN) {
                coordinateX.setStartY(coordinateX.getStartY()-5);
                coordinateX.setEndY(coordinateX.getEndY()-5);
            }
        }

    }
}